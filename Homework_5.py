import fake as fake
import fields as fields
from webargs import fields
from flask import Flask
from marshmallow import validate
from flask_apispec import use_kwargs
from db import execute_query
from utils import format_records
import re
from faker import Faker
from faker.providers import company

app = Flask(__name__)


@app.route("/")
def hello_hw():
    return "Homework 5"


@app.route('/unique_names')
def get_names():
    query = 'select count(distinct FirstName) as count from customers c'
    records = execute_query(query)
    result = format_records(records)
    return result


@app.route('/tracks_count')
def get_tracks_count():
    query = 'select count(*) from tracks t'
    records = execute_query(query)
    result = format_records(records)
    return result

#
# @app.route('/customers')
# @use_kwargs(
#     {
#         "text": fields.Str(
#             required=False,
#             missing=None,
#         ),
#     },
#     location="query",
# )
# def get_customers(text):
#     query = 'select * from customers'
#
#     text_fields = ['FirstName', 'LastName', 'Company', 'Address', 'City', 'State', 'Country', 'Email']
#
#     if text:
#         query += ' WHERE ' + ' OR '.join(f'{field} like ?' for field in text_fields)
#
#     records = execute_query(query, (f'%{text}%',) * len(text_fields))
#     result = format_records(records)
#
#     return result


@app.route('/fill_companies')
def fill_companies():
    query = 'select company from customers c where company is null'
    fake = Faker()
    for c in query:
        c = f'insert into customers(companies) values {fake.company()}'
    records = execute_query(query)
    result = format_records(records)
    return result


@app.route('/sales')
def get_sales():
    query = 'select sum(UnitPrice*Quantity) as Sales from invoice_items ii '
    records = execute_query(query)
    result = format_records(records)
    return result


app.run(port=5000, debug=True)
