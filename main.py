from wsgiref import validate

from flask import Flask
from marshmallow import fields

from db import execute_query
from utils import format_records

app = Flask(__name__)


@app.route("/")
def hello_hw():
    return "SQL learning"


@app.route('/customers')
@use_kwargs(
    {
        "first_name": fields.Str(
            required=False,
            missing=None,
            validate=[validate.Regexp('^[0-9]')]
        ),
        "last_name": fields.Str(
            required=False,
            missing=None,
            validate=[validate.Regexp('[a-zA-Z]*')]
        ),
    },
    location="query",
)
def get_customers(first_name, last_name):
    query = 'select * from customers'

    fields = {}
    if first_name:
        fields["FirstName"] = first_name

    if last_name:
        fields["LastName"] = last_name

    if fields:
        query += ' WHERE ' + ' AND '.join(f'{k}="{v}"' for k, v in fields.items())

    records = execute_query(query)
    result = format_records(records)
    return result

app.run(port=5002, debug=True)


class colorizer:
    def __init__(self, grey, red, yellow, blue, purple, white, bold, default, underline):
        self.grey = '\033[90m'
        self.red = '\033[91m'
        self.yellow = '\033[93m'
        self.blue = '\033[94m'
        self.purple = '\033[95m'
        self.white = '\033[97m'
        self.bold = '\033[1m'
        self.default = '\033[0m'
        self.underline = '\033[4m'






# assert(list(frange(5)) == [0, 1, 2, 3, 4])
# assert(list(frange(2, 5)) == [2, 3, 4])
# assert(list(frange(2, 10, 2)) == [2, 4, 6, 8])
# assert(list(frange(10, 2, -2)) == [10, 8, 6, 4])
# assert(list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
# assert(list(frange(1, 5)) == [1, 2, 3, 4])
# assert(list(frange(0, 5)) == [0, 1, 2, 3, 4])
# assert(list(frange(0, 0)) == [])
# assert(list(frange(100, 0)) == [])