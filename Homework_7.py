import math


class Shape:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def square(self):
        return 0


class Point(Shape):
    def __init__(self, x, y):
        super().__init__(x, y)


class Circle(Shape):
    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    def square(self):
        return math.pi * self.radius ** 2


class Rectangle(Shape):
    def __init__(self, x, y, height, width):
        super().__init__(x, y)
        self.height = height
        self.width = width

    def square(self):
        return self.width * self.height


class Parallelogram(Rectangle):
    def __init__(self, x, y, height, width, angle):
        super().__init__(x, y, height, width)
        self.angle = angle

    def square(self):
        return self.x * self.y * math.sin(self.angle)


class Triangle(Shape):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z

    def square(self):
        p = (self.x + self.y + self.z) / 2
        return math.sqrt(p * (p - self.x) * (p - self.y) * (p - self.z))


p = Point(5, 23)
c = Circle(0, 1, 5)
r = Rectangle(3, 4, 7, 6)
p = Parallelogram(1, 2, 3, 4, 45)
t = Triangle(5, 6, 7)
print(f' Площадь круга {c.square()}')
print(f' Площадь четырехугольника {r.square()}')
print(f' Площадь параллелограма {p.square()}')
print(f' Площадь треугольника {t.square()}')


class colorizer:
    def __init__(self, colour, red='\033[91m', yellow='\033[93m', blue='\033[94m',
                 purple='\033[95m', white='\033[97m', default='\033[0m'):
        self.red = '\033[91m'
        self.default = '\033[0m'
        self.yellow = '\033[93m'
        self.blue = '\033[94m'
        self.purple = '\033[95m'
        self.white = '\033[97m'
        self.colour = colour

    def __enter__(self):
        if self.colour == 'red':
            print(self.red)
        elif self.colour == 'yellow':
            print(self.yellow)
        elif self.colour == 'blue':
            print(self.blue)
        elif self.colour == 'purple':
            print(self.purple)
        elif self.colour == 'white':
            print(self.white)
        else:
            print('Sorry, I didn\'t expect that colour')

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(self.default)


with colorizer(colour='blue'):
    print('Printed in blue')
    print('Again in blue')
print('Printed in default color')
with colorizer(colour='purple'):
    print('Printed in purple')
print('Printed in default color')
with colorizer(colour='yellow'):
    print('Printed in yellow')
with colorizer(colour='red'):
    print('Printed in red')
print('Printed in default color')


class frange:
    def __init__(self, left, right, step=1):
        self._left = float(left)
        self._right = float(right)
        self._step = float(step)

    def __next__(self):
        if self._left + self._step > self._right:
            raise StopIteration('STOP')
        result = self._left
        self._left += self._step
        return result

    def __iter__(self):
        return self


# assert(list(frange(5)) == [0, 1, 2, 3, 4])
# assert(list(frange(2, 5)) == [2, 3, 4])
# assert(list(frange(2, 10, 2)) == [2, 4, 6, 8])
# assert(list(frange(10, 2, -2)) == [10, 8, 6, 4])
# assert(list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
# assert(list(frange(1, 5)) == [1, 2, 3, 4])
# assert(list(frange(0, 5)) == [0, 1, 2, 3, 4])
# assert(list(frange(0, 0)) == [])
# assert(list(frange(100, 0)) == [])
