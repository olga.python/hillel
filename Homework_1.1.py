import time
import functools
import requests

from collections import OrderedDict, Counter


def profile(msg='Elapsed time'):
    def internal(f):
        @functools.wraps(f)
        def deco(*args, **kwargs):
            start = time.time()
            result = f(*args, **kwargs)
            print(msg, f'({f.__name__}): {time.time() - start}s')
            return result

        return deco

    return internal


def cache(max_limit=64):
    def internal(f):
        @functools.wraps(f)
        def deco(*args):
            # If its already in cache, adding it to the list with keys
            if args in deco._cache:
                # To the end
                deco._mentions.append(args)
                return deco._cache[args]

            result = f(*args)

            # If cache is full with max_limit elements
            if len(deco._cache) >= max_limit:
                # Identifying the least used key
                least_used = Counter(deco._mentions).most_common()[-1](0)
                del deco._cache[least_used]

            deco._mentions.append(args)
            deco._cache[args] = result
            return result

        deco._cache = OrderedDict
        deco._mentions = []
        return deco

    return internal



@profile(msg='Elapsed time')
@cache(max_limit=7)
def fetch_url(url, first_n=100):
    """Fetch a given url"""
    res = requests.get(url)
    return res.content[:first_n] if first_n else res.content

#1-time used ain, reddit, google
fetch_url('https://google.com')
fetch_url('https://google.com')
fetch_url('https://ithillel.ua')
fetch_url('https://ithillel.ua')
fetch_url('https://ithillel.ua')
fetch_url('https://dou.ua')
fetch_url('https://dou.ua')
fetch_url('https://ain.ua')
fetch_url('https://youtube.com')
fetch_url('https://youtube.com')
fetch_url('https://reddit.com')
fetch_url('https://google.com')
