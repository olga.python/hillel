import datetime
from django.db import models
import uuid


class Course(models.Model):
    id = models.UUIDField(
        primary_key=True, unique=True, max_length=16, default=uuid.uuid4, editable=False
    )
    name = models.CharField(null=False, max_length=100)
    start_date = models.DateField(null=True, default=datetime.date(2021, 10, 1))
    count_of_students = models.IntegerField(default=0)
    room = models.ForeignKey(
        "courses.Room", null=True, related_name="courses", on_delete=models.SET_NULL
    )

    def __str__(self):
        return f"{self.name}"


class Room(models.Model):
    location = models.CharField(
        max_length=100,
        null=True,
        blank=True,
    )
    color = models.ForeignKey("courses.Color", null=True, on_delete=models.SET_NULL)


class Color(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
