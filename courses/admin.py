from django.contrib import admin
from .models import Course, Room, Color

admin.site.register(Course)
admin.site.register(Room)
admin.site.register(Color)
