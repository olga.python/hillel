from django.urls import reverse_lazy
from courses.models import *
from webargs import djangoparser
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin


parser = djangoparser.DjangoParser()


class CourseList(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("students:login")
    model = Course
    context_object_name = "courses_list"
    template_name = "courses_table.html"
