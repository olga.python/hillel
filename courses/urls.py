from django.urls import path

from courses.views import (
    CourseList,
)

app_name = "courses"

urlpatterns = [
    path("", CourseList.as_view(), name="courses-list"),
]
