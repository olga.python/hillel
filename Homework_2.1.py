import csv
from faker import Faker
from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "<p>Homework for lesson 2 in action!</p>"


@app.route('/pipfile')
def get_pipfile():
    with open('Pipfile.lock', 'r') as file:
        pip = file.readlines()
        file.close()

    return (str(pip))


@app.route('/random_students')
def get_random_students():
    Faker.seed(0)
    f = Faker(['en_US'])
    students = [f.name() for _ in range(5)]

    return ('***'.join(students))


@app.route('/avr_data')
def get_avr_data(*args):
    with open('hw.csv') as f:
        reader = csv.DictReader(f, delimiter=",")
        total_height = []
        total_weight = []
        for row in reader:
            total_height += float(row['"Height(Inches)"'])
            total_weight += float(row['"Weight(Pounds)"'])
            avr_height = (total_height/row)
            avr_weight = (total_weight/row)
            f.close()
# ПЕРЕВЕДИ В СМ И КГ
            return avr_weight, avr_height

    return f'The average height is {avr_height}, the average weight is {avr_weight}'


app.run(debug=True)
