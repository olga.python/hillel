from django.contrib import admin
from django.utils.html import format_html
from .models import Teacher


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'course_count', 'list_courses']

    def list_courses(self, obj):
        if obj.course:
            courses = obj.course.all()
            links = [
                f"<a href='http://127.0.0.1:8000/admin/students/course/{course.pk}/change/'>{course.name}</p>"
                for course in courses]

            return format_html(f"{''.join(links)}")
        else:
            return format_html("Empty courses")

    def course_count(self, obj):
        if obj.course:
            courses = obj.course.all().count()
            return format_html(f"<p>{courses}</p>")
        else:
            return format_html(f"<p>0</p>")
