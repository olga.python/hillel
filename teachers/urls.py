from django.urls import path

from teachers.views import (
    TeachersList,
    CreateTeacher,
)

app_name = "teachers"

urlpatterns = [
    path("", TeachersList.as_view(), name="teachers-list"),
    path("create/", CreateTeacher.as_view(), name="create-teacher"),
]
