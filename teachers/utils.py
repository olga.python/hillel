def format_records(records):
    if not records:
        return "(Empty recordset)"
    return "<br>".join(
        f'<a href="/teachers/update/{rec.id}/">EDIT</a> {rec}' for rec in records
    )
