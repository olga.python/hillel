from django.views.generic import CreateView, ListView
from courses.models import Course
from students.models import *
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin


class TeachersList(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("students:login")
    model = Teacher
    template_name = "teachers_table.html"
    context_object_name = "teachers_list"
    extra_context = {
        "courses_list": Course.objects.all(),
    }

    def get_queryset(self):
        query = self.request.GET.get("course")
        print(Teacher.objects.filter(course=query))
        if query:
            self.extra_context['teachers_list'] = Teacher.objects.filter(course=query)
            return Teacher.objects.filter(course=query)
        return Teacher.objects.all()


class CreateTeacher(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("students:login")
    template_name = "teacher_create.html"
    fields = "first_name", "last_name", "course"
    model = Teacher
    success_url = reverse_lazy("teachers:teachers-list")
