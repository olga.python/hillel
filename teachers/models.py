from django.db import models
from django.core.validators import MinLengthValidator
from django.db import models


class Teacher(models.Model):
    first_name = models.CharField(
        max_length=60, null=False, validators=[MinLengthValidator(2)]
    )
    last_name = models.CharField(
        max_length=80, null=False, validators=[MinLengthValidator(2)]
    )
    course = models.ManyToManyField(to="courses.course", related_name="teachers")

    def __str__(self):
        return f"{self.full_name()}"

    def full_name(self):
        return f"{self.first_name} {self.last_name}"
