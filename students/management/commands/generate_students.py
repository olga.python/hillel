import string
from random import random, randint

from django.core.management.base import BaseCommand
from faker import Faker

from students.models import Student


class Command(BaseCommand):
    help = "Generates N number of fake students"

    def handle(self, *args, **kwargs):
        faker = Faker()
        count = kwargs["count"]
        for _ in range(count):
            st = Student(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birthdate=faker.date_time_between(start_date="-30y", end_date="-18y"),
                phone_number=f"+390{faker.msisdn()[3:]}",
            )
            st.save()
        self.stdout.write(
            self.style.SUCCESS(f"Successfully added students count {count}")
        )

    def add_arguments(self, parser):
        parser.add_argument(
            "count", help="The number of students to generate", type=int
        )
