from django.apps import AppConfig
from suit.apps import DjangoSuitConfig


class StudentsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "students"

    def ready(self):
        import students.signals  # noqa


class SuitConfig(DjangoSuitConfig):
    layout = 'horizontal'
