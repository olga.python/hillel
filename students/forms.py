from django.core.validators import ValidationError
from students.models import *
import datetime
from django.forms import ModelForm, TextInput, EmailField
from django.contrib.auth.forms import UserCreationForm


class PersonBaseForm(ModelForm):
    class Meta:
        model = Student
        fields = [
            "first_name",
            "last_name",
            "email",
            "birthdate",
            "phone_number"
        ]
        widgets = {"phone_number": TextInput(attrs={"pattern": "\d{10,14}"})}

    @staticmethod
    def normalize_name(name):
        return name.lower().strip().capitalize()

    def clean_first_name(self):
        first_name = self.cleaned_data["first_name"]

        return self.normalize_name(first_name)

    def clean_last_name(self):
        last_name = self.cleaned_data["last_name"]

        return self.normalize_name(last_name)

    def clean(self):
        cleaned_data = super().clean()

        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]

        if first_name == last_name:
            raise ValidationError(
                "Maybe you made a mistake, because your last name "
                "is equal to first name"
            )
        return cleaned_data

    def clean_birthdate(self):
        birthdate = self.cleaned_data["birthdate"]

        if datetime.datetime.now().year - self.birthdate.year < 18:
            raise ValidationError("You are too young to be our student, sorry")

        return birthdate


class StudentCreateForm(PersonBaseForm):
    class Meta(PersonBaseForm.Meta):
        fields = [
            "first_name",
            "last_name",
            "avatar",
            "email",
            "phone_number",
            "resume",
        ]


class RegistrationStudentForm(UserCreationForm):
    email = EmailField(
        max_length=200, help_text="Registration without email is not possible!"
    )

    class Meta:
        model = CustomUser
        fields = ["email", "password1", "password2"]
