from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions


def no_elon_validator(email):
    if "elon" in email.lower():
        raise ValidationError("No more Elons, please!")


def secure_email_validator(email):
    RESTRICTED = [
        "@gmail.com",
        "@yandex.ru",
        "@mail.ru",
        "@outlook.com",
        "@example.net",
        "@example.org",
    ]
    if not any([email.endswith(e) for e in RESTRICTED]):
        raise ValidationError("Enter correct email, please")


def file_extension(resume):
    import os
    from django.core.exceptions import ValidationError

    ext = os.path.splitext(resume.name)[1]
    valid_extensions = [".pdf", ".txt", ".word"]
    if not ext.lower() in valid_extensions:
        raise ValidationError(
            "Unsupported file extension. We accept only txt/doc/pdf files"
        )


def clean_avatar(avatar):
    try:
        main, sub = avatar.content_type.split("/")
        if not (main == "image" and sub in ["jpeg", "gif", "png"]):
            raise ValidationError("Please use a JPEG, GIF or PNG image.")
        if len(avatar) > (20 * 1024):
            raise ValidationError("Avatar file size may not exceed 20k.")
    except AttributeError:
        pass
    return avatar
