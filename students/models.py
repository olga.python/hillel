from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.mail import send_mail
from faker import Faker
from students.managers import PeopleManager
from students.validators import (
    no_elon_validator,
    secure_email_validator,
    clean_avatar,
    file_extension,
)
from teachers.models import *
from django.db import models
import datetime
from students.managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('email address', unique=True)
    first_name = models.CharField('first name', max_length=150, blank=True)
    last_name = models.CharField('last name', max_length=150, blank=True)
    date_joined = models.DateTimeField('date joined', auto_now_add=True)
    photo = models.ImageField(upload_to='user_photos/', null=True, blank=True)
    is_active = models.BooleanField(
        'active',
        default=True,
        help_text='Designates whether this user should be treated as active. '
                  'Unselect this instead of deleting accounts.',
    )
    is_staff = models.BooleanField(
        'staff status',
        default=False,
        help_text='Designates whether the user can log into this admin site.',
    )
    objects = CustomUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = f'{self.first_name} {self.last_name}'
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class ExtendedUser(CustomUser):
    people = PeopleManager()

    class Meta:
        proxy = True
        ordering = ('first_name',)

    def some_action(self):
        print(self.username)


class Person(models.Model):
    first_name = models.CharField(
        max_length=60, null=False, validators=[MinLengthValidator(2)]
    )
    last_name = models.CharField(
        max_length=80, null=False, validators=[MinLengthValidator(2)]
    )
    email = models.EmailField(
        max_length=120,
        null=True,
        validators=[no_elon_validator, secure_email_validator],
    )
    phone_number = models.CharField(null=True, max_length=14, unique=True)

    class Meta:
        abstract = True
        verbose_name = "My student"
        verbose_name_plural = "All students"


class Student(Person):
    birthdate = models.DateField(null=True, default=datetime.date.today)
    course = models.ForeignKey(
        "courses.course", null=True, related_name="students", on_delete=models.SET_NULL
    )
    avatar = models.ImageField(
        upload_to="avatars/", null=True, blank=True, validators=[clean_avatar]
    )
    resume = models.FileField(
        upload_to="resumes/", null=True, blank=True, validators=[file_extension]
    )
    invitations = models.IntegerField(null=True, default=0)

    def __str__(self):
        return f"{self.full_name()}, {self.age()} ({self.id})"

    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def age(self):
        return datetime.datetime.now().year - self.birthdate.year

    def clean_first_name(self):
        first_name = self.cleaned_data["first_name"]

        return first_name.lower().strip().capitalize()

    def clean_last_name(self):
        last_name = self.cleaned_data["last_name"]

        return last_name.lower().strip().capitalize()

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            st = cls(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birthdate=faker.date_time_between(start_date="-30y", end_date="-18y"),
            )
            st.save()


class Invitations(models.Model):
    student = models.ForeignKey(
        "students.invitations", null=True, on_delete=models.CASCADE
    )
    invite_code = models.CharField(max_length=120, null=False)
    invited_email = models.EmailField(
        max_length=120,
        null=True,
        validators=[no_elon_validator, secure_email_validator],
    )


class UserProfile(Person, models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    course = models.CharField(max_length=60, null=True)
    CHOICES = [
        (1, 'Student'),
        (2, 'Teacher'),
    ]

    def __str__(self):
        return f"{self.user.first_name}_{self.user.last_name}"
