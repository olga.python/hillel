import self as self
from django.contrib.auth import login
from django.db.models import Q
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.views import LoginView, LogoutView
from django.forms.utils import ErrorList
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail, EmailMessage
from django.contrib import messages

from courses.models import Course
from students.forms import *
from students.models import *
from django.core.exceptions import BadRequest
from webargs import djangoparser
from django.views.generic import (
    ListView,
    TemplateView,
    CreateView,
    UpdateView,
    DeleteView, RedirectView,
)
from students.services.emails import send_registration_email
from students.token_generator import TokenGenerator

parser = djangoparser.DjangoParser()


def hello(request):
    return HttpResponse("SUCCESS")


@parser.error_handler
def handle_error(error, req, schema, *, error_status_code, error_headers):
    raise BadRequest(error.messages)


class StudentList(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("students:login")
    model = Student
    template_name = "students_table.html"
    extra_context = {
        "courses_list": Course.objects.all(),
        "students_list": Student.objects.all(),
    }

    def get_queryset(self):
        query = self.request.GET.get("course_id")
        print(Student.objects.filter(course_id=query))
        if query:
            self.extra_context['students_list'] = Student.objects.filter(course_id=query)
            return Student.objects.filter(course_id=query)
        return Student.objects.all()


class SearchStudents(ListView):
    model = Student
    template_name = "students_table.html"
    context_object_name = "students_list"
    extra_context = {
        "courses_list": Course.objects.all(),
    }

    def get_queryset(self):
        query = self.request.GET.get("search")
        students_list = Student.objects.filter(
            Q(first_name__icontains=query) | Q(last_name__icontains=query)
        )
        return students_list


class UpdateStudent(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy("students:login")
    model = Student
    template_name = "students_update.html"
    fields = (
        "first_name",
        "last_name",
        "email",
        "phone_number",
        "course",
        "avatar",
        "resume",
    )
    success_url = reverse_lazy("students:list")


class DeleteStudent(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("students:login")
    model = Student
    template_name = "students_delete.html"
    success_url = reverse_lazy("students:list")


class CreateStudent(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("students:login")
    template_name = "students_create.html"
    fields = (
        "first_name",
        "last_name",
        "email",
        "phone_number",
        "birthdate",
        "course",
        "avatar",
        "resume",
    )
    model = Student
    success_url = reverse_lazy("students:list")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        if first_name == last_name:
            form._errors["first_name"] = ErrorList(
                ["First and last name can't be equal"]
            )
            form._errors["last_name"] = ErrorList(
                ["First and last name can't be equal"]
            )
            return super().form_invalid(form)
        return super().form_valid(form)


class LoginStudent(LoginView):
    success_url = reverse_lazy("index")


class LogoutStudent(LogoutView):
    template_name = "registration/logout.html"


class RegistrationStudent(CreateView):
    form_class = RegistrationStudentForm
    template_name = "registration/registration.html"
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(request=self.request,
                                user_instance=self.object)
        return super().form_valid(form)


class IndexPage(LoginRequiredMixin, TemplateView):
    template_name = "index.html"
    login_url = reverse_lazy("students:login")
    extra_context = {"courses_list": Course.objects.all()}


def send_email(request):
    email = EmailMessage(subject='Registration from LMS',
                         body="Test text",
                         to=['lms.students.hw@gmail.com'])
    email.send()
    return HttpResponse('Done')


class ActivateUser(RedirectView):
    url = reverse_lazy("students:login")

    def get(self, request, uidb64, token, *args, **kwargs):
        print(f"uidb64: {uidb64}")
        print(f"token: {token}")

        try:
            user_pk = force_bytes(urlsafe_base64_decode(uidb64))
            print(f"user_pk: {user_pk}")
            current_user = CustomUser.objects.get(pk=user_pk)
        except (CustomUser.DoesNotExist, ValueError, TypeError):
            return HttpResponse("Wrong data")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)
            return super().get(request, *args, **kwargs)
        return HttpResponse("Wrong data")
