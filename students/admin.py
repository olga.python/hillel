from django.contrib import admin
from django.contrib.admin.options import InlineModelAdmin

from .models import Student, Invitations, UserProfile, CustomUser


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'age', 'email', 'phone_number']
    search_fields = ['first_name__startswith', 'last_name__startswith']
    list_filter = ['first_name']
    extra = 1


class UserProfileAdmin(admin.StackedInline):
    model = UserProfile
    extra = 0


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    inlines = [UserProfileAdmin]


admin.site.register(Invitations)
