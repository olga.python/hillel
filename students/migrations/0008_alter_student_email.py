# Generated by Django 3.2.8 on 2021-10-18 13:06

from django.db import migrations, models
import students.validators


class Migration(migrations.Migration):

    dependencies = [
        ("students", "0007_student_enroll_date_student_graduate_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="student",
            name="email",
            field=models.EmailField(
                max_length=120,
                null=True,
                unique=True,
                validators=[
                    students.validators.no_elon_validator,
                    students.validators.secure_email_validator,
                ],
            ),
        ),
    ]
