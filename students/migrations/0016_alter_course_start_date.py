# Generated by Django 4.0a1 on 2021-10-27 19:06

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("students", "0015_invitations"),
    ]

    operations = [
        migrations.AlterField(
            model_name="course",
            name="start_date",
            field=models.DateField(default=datetime.date(2021, 10, 27), null=True),
        ),
    ]
