from flask import Flask
from db import execute_query
from utils import format_records
from marshmallow import validate
from webargs import fields
from flask_apispec import use_kwargs



app = Flask(__name__)


@app.route("/")
def hello_hw():
    return "Homework 6 "


@app.route('/genres_durations')
def get_genre_durations():
    query = 'select genres.GenreId, genres.Name , (sum(Milliseconds)/1000) as duration_sec' \
            'from tracks t join genres on t.GenreId = genres.GenreId' \
            'group by t.GenreId'
    records = execute_query(query)
    result = format_records(records)
    return result


@app.route("/greatest_hits")
@use_kwargs(
    {
        "count": fields.Str(
            required=False,
            missing=None,
            validate=[validate.Regexp('[0-9]*')]
        ),
    },
    location="query",
)
def get_greatest_hits(count):
    query = 'SELECT t.Name, t.TrackId , ii.Quantity, ii.UnitPrice , count(*) as N ' \
            'from invoice_items ii join tracks t on ii.TrackId == t.TrackId order by t.Name '
    if count:
        query += f' limit {count} '

    records = execute_query(query)
    result = format_records(records)
    return result


@app.route("/greatest_artists")
@use_kwargs(
    {
        "count": fields.Str(
            required=False,
            missing=None,
            validate=[validate.Regexp('[0-9]*')]
        ),
    },
    location="query",
)
def get_greatest_artists(count):
    query = 'SELECT ar.* , al.* , t.* , ii.*, sum(ii.UnitPrice*ii.Quantity) as Sales '\
            'from artists ar join albums al on ar.ArtistId == al.ArtistId ' \
            'join tracks t on al.AlbumId == t.AlbumId '\
            'join invoice_items ii on t.TrackId == ii.TrackId '\
            'GROUP BY ar.Name ORDER BY ar.Name , Sales'

    if count:
        query += f' limit {count} '

    records = execute_query(query)
    result = format_records(records)
    return result


@app.route("/stats_by_city")
@use_kwargs(
    {
        "genres": fields.Str(
            required=False,
            missing='Hip-Hop',
            validate=[validate.Regexp('^[0-9]*')]
        ),
        "count": fields.Str(
            required=False,
            missing=None,
            validate=[validate.Regexp('[0-9]*')]
        ),
    },
    location="query",
)
def get_stats_by_city(genres, count):
    query = 'SELECT c.* , i.* , ii.* , t.* ' \
            'from customers c join invoices i on c.CustomerId == i.CustomerId ' \
            'join invoice_items ii on i.InvoiceId == ii.InvoiceId ' \
            'join tracks t on ii.TrackId == t.TrackId ' \
            'join genres g on t.GenreId == g.GenreId ' \
            'group by g.GenreId ORDER by c.City'

    if count:
        query += f' limit {count} '

    records = execute_query(query)
    result = format_records(records)
    return result


app.run(port=5006, debug=True)


class Point:
    def __init__(self, x_point, y_point):
        self.x_point = x_point
        self.y_point = y_point


class Circle:
    def __init__(self, center_x, center_y, radius):
        self.center_x = center_x
        self.center_y = center_y
        self.radius = radius

    def contains(self, point: Point):
        is_inside = 0
        square = (self.center_x - point.x_point) ** 2 + (self.center_y - point.y_point) ** 2
        is_inside = square <= self.radius ** 2
        return is_inside


p = Point(5, 23)
c = Circle(0, 0, 5)
print(c.contains(p))
