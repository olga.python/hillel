import json
import random
import string
import requests
from flask import request
from flask import Flask

from marshmallow import validate
from webargs import fields
from webargs.flaskparser import use_kwargs

app = Flask(__name__)


@app.route("/")
def hello_hw():
    return "Homework for lesson 3 is an action"


@app.route('/password')
@use_kwargs({
    "length": fields.Int(
        missing=10,
        validate=[validate.Range(min=1, max=100)]),
    "specials": fields.Bool(
        truthy=random.choice("!\"№;%:?*$()_+"),
        falsy=None,
        missing=False,
        validate=[validate.Range(min=0, max=1)]),
    "digits": fields.Bool(
        truthy=random.randint(0, 9),
        falsy=None,
        missing=False,
        validate=[validate.Range(min=0, max=1)]
    )},
    location="query")
def generate_password(length, specials, digits):
    random_base = string.ascii_lowercase + string.ascii_uppercase
    if digits:
        random_base += string.digits
    if specials:
        random_base += string.specials
    return ''.join(random.choices(random_base, k=int(length)))


@app.route('/bitcoin_rate')
@use_kwargs({
    "currency": fields.Str(
        missing="USD",
        validate=[validate.Length(min=2, max=3)]
    )
})

def get_bitcoin_rate(currency):
    url = 'https://bitpay.com/api/rates'
    res = requests.get(url)
    result = res.json()
    value = None
    for item in result:
        if item['code'] == currency:
            value = item['rate']
            break
    return f'The rate is {value}'


app.run(port=5001, debug=True)

